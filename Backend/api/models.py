import uuid
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
class Venue(models.Model):
    id = models.AutoField(primary_key=True)
    name=models.CharField(max_length=50)
    computers=models.IntegerField()
    has_compters=models.BooleanField()

    def __str__(self) -> str:
        return self.name

class Booking(models.Model):
    id = models.AutoField(primary_key=True)
    book_date=models.DateTimeField( auto_now_add=True)
    date=models.DateField(null=True)
    start_time=models.TimeField(null=True)
    end_time=models.TimeField(null=True)
    purpose=models.CharField( max_length=50,default='Studying')
    coins=models.FloatField(default=0)
    points=models.FloatField(default=0)
    computers=models.IntegerField(default=0,validators=[MinValueValidator(0)])
    description=models.CharField(max_length=100,null=True)
    participants=models.IntegerField(default=0)
    duration_hours=models.FloatField(default=0)
    reference_no=models.CharField(max_length=6,null=True,unique=True, default=uuid.uuid1)
    status=models.CharField(max_length=20,null=True, default='Booked')
    user=models.CharField(max_length=100,null=True)
    venue=models.ForeignKey(Venue,related_name='venue',to_field='id',on_delete=models.CASCADE)
    user_id=models.IntegerField(default=0)
    def __str__(self) -> str:
        return f"REF NO:{self.reference_no}"

class Attendee(models.Model):
    id = models.AutoField(primary_key=True)
    name=models.CharField(max_length=50,null=True)
    booking=models.ForeignKey(Booking,related_name='attendees',on_delete=models.CASCADE,null=True)   
    
    def __str__(self) -> str:
        return f"{self.name}"    

