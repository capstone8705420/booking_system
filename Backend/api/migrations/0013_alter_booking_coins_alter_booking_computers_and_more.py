# Generated by Django 4.2.1 on 2023-05-09 01:38

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0012_alter_attendee_booking'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='coins',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='booking',
            name='computers',
            field=models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0)]),
        ),
        migrations.AlterField(
            model_name='booking',
            name='duration_hours',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='booking',
            name='points',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='booking',
            name='purpose',
            field=models.CharField(default='Studying', max_length=50),
        ),
    ]
