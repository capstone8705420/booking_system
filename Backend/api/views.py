from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse
from rest_framework.response import Response
from rest_framework import *
from rest_framework.views import APIView
from rest_framework import status
from api.models import Booking,Venue
from api.serializers import BookingSerializer, VenueSerializer,BookingRequestSerializer
from datetime import datetime, date
# Create your views here.

class BookingDetail(APIView):
    def get(self,request):
        obj = Booking.objects.all()
        serializer = BookingSerializer(obj,many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)

    def post(self, request):        
        
        serializer= BookingRequestSerializer(data=request.data)    
        
        if serializer.is_valid():
            date=serializer.validated_data['date']
            # calculate hours_difference
            startTime=serializer.validated_data['start_time']
            endTime=serializer.validated_data['end_time']
            duration=datetime.combine(date,endTime)-datetime.combine(date,startTime)
            hours_difference = (duration).total_seconds()/3600.0
            serializer._validated_data['duration_hours']=hours_difference
            # set status to booked
            serializer._validated_data['status']='Booked'
            serializer.save()
            response_data={
                'message':'Succesfully booked'
            }
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        return Response(serializer.data,status=status.HTTP_400_BAD_REQUEST)\

class AttendanceDetail(APIView):
    def get(self,request):
        obj = Booking.objects.filter(id=request.pk)
        serializer = BookingSerializer(obj,many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)