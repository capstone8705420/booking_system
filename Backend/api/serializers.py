import datetime
from rest_framework import serializers
from api.models import Booking,Venue,Attendee

class BookingSerializer(serializers.ModelSerializer):
    class Meta:
        model=Booking
        fields=('__all__')

class VenueSerializer(serializers.ModelSerializer):
    class Meta:
        model=Venue
        fields=('__all__')
class AttendeeSerializer(serializers.ModelSerializer):
    class Meta:
        model=Attendee
        fields=('__all__')

class BookingRequestSerializer(serializers.ModelSerializer):
    attendees=AttendeeSerializer(many=True)
    class Meta:
        model=Booking
        fields=('purpose','description','venue','date','start_time','end_time','computers','participants','coins','points','user','attendees')
    def create(self, validated_data):
        attendees=validated_data.pop('attendees')
        booking=Booking(**validated_data)        
        booking.save()
        serializer= AttendeeSerializer(data=attendees,many=True)
        if serializer.is_valid(raise_exception=True):
            attendees=serializer.save(booking=booking)
        return booking
    